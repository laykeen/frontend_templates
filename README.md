# frontend_templates

Various templates made with various libraries like Bootstrap, ecc. or done with handmade styles.

## How this repo works

#### Categories

- web pages divided by categories
	- shop
	- forum
	- wiki
	- ...

#### Testing

- scripts and web pages divided by subjects
	- color palettes
	- php scripts
	- js scripts
	- ...

## Contributing

TODO

### Used libraries

TODO

## License

TODO
